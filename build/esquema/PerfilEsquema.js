"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
//Se colocan los nombres de las columnas de la tabla o coleccion
const PerfilEsquema = new mongoose_1.Schema({
    nombrePerfil: { type: String, required: true, unique: true, trim: true },
    estadoPerfil: { type: Number, enum: [1, 2, 3], default: 1 }
}, { versionKey: false });
//Esta linea no permite que se agrege al final una s a las colecciones
exports.default = (0, mongoose_1.model)("Perfil", PerfilEsquema, "Perfil");
