"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UsuarioEntidad {
    constructor(nomu, estu, coru, clau, fecu, ava, codp) {
        //nomp = nombre perfil
        //estp = estado perfil
        this.nombreUsuario = nomu;
        this.estadoUsuario = estu;
        this.correoUsuario = coru;
        this.claveUsuario = clau;
        this.fechaCreacionUsuario = fecu;
        this.avatarUsuario = ava;
        this.codPerfil = codp;
    }
    ;
}
;
exports.default = UsuarioEntidad;
