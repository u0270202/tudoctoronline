"use strict";
//permite acceso a la aplicacion al que tiene el token
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
class Seguridad {
    //Recibir-responder-dar el paso
    analizarToken(req, res, next) {
        var _a;
        //Estas autorizado?
        if (req.headers.authorization) {
            try {
                const miLlavesita = String(process.env.CLAVE_SECRETA);
                const tokenRecibido = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(" ")[1];
                const infoUsuario = jsonwebtoken_1.default.verify(tokenRecibido, miLlavesita);
                req.body.datosUsuario = infoUsuario;
                next();
            }
            catch (error) {
                res.status(401).json({ respuesta: "el token no es correcto" });
            }
        }
        else {
            res.status(401).json({ respuesta: "No tienes el token" });
        }
    }
}
const seguridad = new Seguridad();
exports.default = seguridad;
