import { model, Schema } from "mongoose";
import PerfilEntidad from "../entidad/PerfilEntidad";
//Se colocan los nombres de las columnas de la tabla o coleccion
const PerfilEsquema = new Schema<PerfilEntidad>({
    nombrePerfil: { type: String, required: true, unique: true, trim: true },
    estadoPerfil: { type: Number, enum: [1, 2, 3], default: 1 }
}, { versionKey: false });

//Esta linea no permite que se agrege al final una s a las colecciones
export default model("Perfil", PerfilEsquema, "Perfil");