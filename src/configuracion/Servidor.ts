import cors from "cors";
import morgan from "morgan";
import dotenv from "dotenv";
import express from "express";
import ConexionDB from "./ConexionDB";
import seguridad from "../middleware/Seguridad";

// Import de las rutas
import perfilRuta from "../ruta/PerfilRuta";
import usuarioRuta from "../ruta/UsuarioRuta";
// ****************************************************

class Servidor {
    public app: express.Application;

    constructor() {
        dotenv.config({ path: "variables.env" });
        ConexionDB();
        this.app = express();
        this.iniciarConfiguracion();
        this.activarRutas();
    }

    public iniciarConfiguracion(): void {
        // this.app.set('PORT', 3100);
        // this.app.set('PORT', process.env.PORT || 3100);
        this.app.set("PORT", process.env.PORT);
        this.app.use(cors());
        this.app.use(morgan("dev"));
        this.app.use(express.json({ limit: "100mb" }));
        this.app.use(express.urlencoded({ extended: true }));
    }

    public activarRutas(): void {
        this.app.use('/api/usuario', usuarioRuta);
        this.app.use('/api/perfil', seguridad.analizarToken, perfilRuta);
    }

    public iniciarServidor(): void {
        this.app.listen(this.app.get("PORT"), () => {
            console.log("servidor funcionando en el puerto: ", this.app.get("PORT"));
        });
    }
};

export default Servidor;