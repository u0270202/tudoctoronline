class PerfilEntidad{
    public nombrePerfil: string;
    public estadoPerfil: number;
    constructor(nomp:string, estp:number){
        //nomp = nombre perfil
        //estp = estado perfil
        this.nombrePerfil = nomp;
        this.estadoPerfil = estp;
    };
};

export default PerfilEntidad;