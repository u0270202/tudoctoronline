import  PerfilEntidad  from './PerfilEntidad';
class UsuarioEntidad{

    public nombreUsuario: string;
    public estadoUsuario: number;
    public correoUsuario: string;
    public claveUsuario: string;
    public fechaCreacionUsuario: Date;
    public avatarUsuario: string;
    public codPerfil: PerfilEntidad;

    constructor(nomu:string, estu:number, coru:string, clau:string, fecu: Date, ava:string, codp: PerfilEntidad ){
        //nomp = nombre perfil
        //estp = estado perfil
        this.nombreUsuario = nomu;
        this.estadoUsuario = estu;
        this.correoUsuario = coru;
        this.claveUsuario = clau;
        this.fechaCreacionUsuario= fecu;
        this.avatarUsuario = ava;
        this.codPerfil = codp;
    };
};

export default UsuarioEntidad;