
//permite acceso a la aplicacion al que tiene el token

import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";


class Seguridad{
    //Recibir-responder-dar el paso
    public analizarToken(req: Request, res: Response, next: NextFunction) {
        //Estas autorizado?
        if (req.headers.authorization) {
            try{
                const miLlavesita = String(process.env.CLAVE_SECRETA);
                const tokenRecibido=req.headers.authorization?.split(" ")[1] as string;
                const infoUsuario=jwt.verify(tokenRecibido,miLlavesita);
                req.body.datosUsuario=infoUsuario;
                next();

            }catch(error){
                res.status(401).json({respuesta:"el token no es correcto"});


            }
            
            
        } else {
            res.status(401).json({respuesta:"No tienes el token"});
            
        }

    }
}


const seguridad = new Seguridad();
export default seguridad; 
